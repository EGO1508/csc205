;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   
;NAME: Clear the RAM
;AUTHOR: Eduardo
;LASTMOD: 10.23.2021
;
;DESCRPTION:
;
;   This is a program that will set all the adresses in the Altair8800 to 0
;   essentially clearing the RAM from all the "garbage" it has and replace it
;   with nice NOP(no operation) instructions. The program works and replaces
;   almost all of the adresses with zero. The only ones that dont
;   get cleared are the ones at the end of the program. This is 
;   because the program self destructs.
; 
;
;   Please be aware that I dont yet understand how to write in this format. 
;   Please correct me if something is not clear. The way I structured the code
;   was that left column is used for the instructions and adresses that go
;   after those instructions. The right column is used to show how the instruction
;   is writen in hex.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


    ORG     0000h   ; Load at memory location zero

    NOP     00h     ; The first two adresses contain what the "garbage" will be replaced with
    NOP     00h     
    LXI     11h     ; Load the register pair DE with the adress where we will start replacing the "garbage"
    0c              ; The values for the LXI instrcution             
    00
    LDA     3ah     ; Load the accumulator with the value specified by the first two adresses
    01
    00
    STAX    12h     ; Store the acumulator contents in the adress specified by register pair DE
    INX     13h     ; Increase register pair vale DE by 1
    JMP     c3h     ; Loop the program
    08
    00

