Introduction
    Logic:

    There was a British mathematician named George Bool that studied the relation ship between logic and math, especially arithmetic. 
    His system of logic assumes that a logical condition is true or false. (Note: Boolean funtion are probably named after George Bool) 
    This is great because when we use binary, something can either be on or be off. There is no in between state, 
    they cant be sort of on or sort of off and they cant be both at the same time (something that is changing with quantum computers).

    From these we can make AND, OR and NOT statements. These produce an output depending on what the input is. These inputs and outputs can be shown in a truth table.

    1 represents True
    0 represents False

    TRUTH TABLE FOR AND:

       INPUT       OUTPUT
      0  |  0  |      0 
      0  |  1  |      0
      1  |  0  |      0 
      1  |  1  |      1         <---- We only get an output of True only if both inputs are True if either input is False we get no output

    TRUTH TABLE FOR OR:
    
       INPUT       OUTPUT
      0  |  0  |      0 
      0  |  1  |      1
      1  |  0  |      1         <---- We get an output of True as long as at least on of the inputs is True
      1  |  1  |      1

    TRUTH TABLE FOR NOT:

        INPUT       OUTPUT
          0      |    1         <---- We get an output that is NOT the input, meaning we get an output that is the opposite of the input
          1      |    0

Electronic Logic:

    All of the basic logic functions we saw earlier can be applied to electronic circuits. These are represented in symbols to help us build logic systems.
    These have an A and B input and an OUTPUT, with the exeption of NOT logic gate that only has one input. If we combine these logic gates, we can get more
    logic gates. Two of these are used often enough that they were assigned their own sybols and logic tables. NAND (NOT-AND) & NOR (NOT-OR).

    Three or more logic circuits make a logic circuit. A basic logic system is the EXCLUSIVE-OR circuit. Its composed of two AND statements,
    one OR statement and one NOT statement. This circuit is often refered as a binary adder. 
    The EXCLUSIVE-OR circuit is often writen as a XOR circuit and has its own symbol.

    NOTE: Minecraft redstone is based on this system of logic. Its possible to create AND, OR, NOT, NOR, NAND, etc, logic gates in game. 
    Its also possible to create XOR circuit in game. Based on this, 
    its possible to create functioning logic circuits in game that visually represents the output.

Number System:

    This sectioin explains that a number system can be based off any number of digits, 
    we humans use the base 10 number system. Computers and logic circuits are very compatible with 
    the binary system. Numbers systems based on 8 and 16 are very usefull with computers because they help us represent lengthy binary numbers, 
    sytems such as Hex for example.

The Binary Sytem:

    This section gives an explanation as to how the binary system works. We only have two digits so when we use up those two, 
    we add another column to represent larger numbers. We can also convert binary to decimal in order to keep the numbers straigh in our head. 
    For me this is better represented graphically to make show it more clearly.

        BINARY     |       DECIMAL
        0                  0
        1                  1
        01                 2            <---- We used up the first column so now we use the next one
        11                 3
        100                4
        101                5
        110                6
        111                7
        1000               8
        1001               9
        1010               10
        1011               11
        1100               12
        1101               13
        1110               14
        1111               15
        10000              16
        10001              17
        10010              18
        10011              19
        10100              20

    It also details how to convert each binary number to decimal. 
    It explains that each bit in a binary number indicates by which power the number is to be raised.
    We start with the number being raised to power 0 and then increasing from there.
